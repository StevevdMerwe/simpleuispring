package za.co.i1solutions.simplicity.config;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {

    final DataSource dataSource;

    public SpringSecurityConfig(DataSource dataSource) {
        super();
        this.dataSource = dataSource;
    }

    //Enable jdbc authentication
    @Autowired
    public void configAuthentication(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication().dataSource(dataSource);
    }

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        http.formLogin()
            .loginPage("/login.html")
            .failureUrl("/login-error.html")
            .and()
            .logout()
            .logoutUrl("/logout.html")
            .logoutSuccessUrl("/index.html")
            .and()
            .authorizeRequests()
            .antMatchers("/user/**").hasRole("USER")
            .and()
            .exceptionHandling()
            .accessDeniedPage("/403.html");
    }
}
