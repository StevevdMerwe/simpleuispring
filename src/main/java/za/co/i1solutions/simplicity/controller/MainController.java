package za.co.i1solutions.simplicity.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.Principal;
import java.util.Locale;

@Controller
@Slf4j
public class MainController {

    @RequestMapping("/")
    public String root(Locale locale) {
        return "redirect:/user/dashboard.html";
    }

    // Login form
    @RequestMapping("/login.html")
    public String login() {
        return "login.html";
    }

    // Login form with error
    @RequestMapping("/login-error.html")
    public String loginError(Model model) {
        log.info("Problem logging in!");
        model.addAttribute("loginError", true);
        return "login.html";
    }

    @RequestMapping("/logout.html")
    public String exit(HttpServletRequest request, HttpServletResponse response) {
        Principal principal = request.getUserPrincipal();
        if (principal != null) {
            log.info("Logging out " + principal.getName());
        }
        new SecurityContextLogoutHandler().logout(request, null, null);
        return "redirect:/login.html";
    }
}
