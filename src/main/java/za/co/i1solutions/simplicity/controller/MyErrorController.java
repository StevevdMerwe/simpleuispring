package za.co.i1solutions.simplicity.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

@Controller
@Slf4j
public class MyErrorController implements ErrorController {

    @RequestMapping("/error")
    public String handleError(HttpServletRequest request) {
        Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);

        if (status != null) {
            Integer statusCode = Integer.valueOf(status.toString());

            try {
                String path = request.getRequestURI();
                String user = "";
                Principal userPrincipal = request.getUserPrincipal();
                if (userPrincipal != null) user = userPrincipal.getName();
                log.error("User {} had a {} error on {}", user, statusCode, path);
            }catch (Exception e){
                log.error("Error handler had an error...");
            }

            if(statusCode == HttpStatus.NOT_FOUND.value()) {
                return "404.html";
            }
            else if(statusCode == HttpStatus.FORBIDDEN.value()) {
                return "403.html";
            }
            else if(statusCode == HttpStatus.INTERNAL_SERVER_ERROR.value()) {
                return "500.html";
            }
        }
        return "error.html";
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }

    /** Simulation of an exception. */
    @RequestMapping("/simulateError.html")
    public void simulateError() {
        throw new RuntimeException("This is a simulated error message");
    }
}
