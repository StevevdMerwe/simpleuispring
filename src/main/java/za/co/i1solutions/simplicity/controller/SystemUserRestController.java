package za.co.i1solutions.simplicity.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import za.co.i1solutions.simplicity.domain.SystemUser;
import za.co.i1solutions.simplicity.repositories.SystemUserRepository;

import java.util.Optional;

@RestController
@RequestMapping("user-rest")
public class SystemUserRestController {

    final
    private SystemUserRepository systemUserRepository;

    public SystemUserRestController(SystemUserRepository systemUserRepository) {
        this.systemUserRepository = systemUserRepository;
    }

    @GetMapping(path ="/{id}", produces = "application/json")
    public SystemUser getBook(@PathVariable Long id) {
        Optional<SystemUser> user = systemUserRepository.findById(id);
        return user.orElse(new SystemUser());
    }
}
