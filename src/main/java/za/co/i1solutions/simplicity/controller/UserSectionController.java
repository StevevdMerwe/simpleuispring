package za.co.i1solutions.simplicity.controller;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

@Controller
public class UserSectionController {

    @GetMapping("/user/project.html")
    public String greetingForm(Model model) {
        return "user/project";
    }

    @GetMapping("/user/tables.html")
    public String tables(Model model) {
        return "user/tables";
    }

    @GetMapping("/user/dashboard.html")
    public String dashboard(Model model) {
        return "user/dashboard";
    }

    @GetMapping("/user/tree.html")
    public String tree(Model model) {
        return "user/tree";
    }

    @GetMapping("/user/modal.html")
    public String modal(Model model) {
        return "user/modal";
    }

    @GetMapping("/user/graph.html")
    public String graph(Model model) {
        return "user/graph";
    }

    @ModelAttribute("userName")
    public String populateName() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication == null ? "Unknown" : authentication.getName();
    }
}
