package za.co.i1solutions.simplicity.repositories;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import za.co.i1solutions.simplicity.domain.SystemUser;

@Repository
public interface SystemUserRepository extends CrudRepository<SystemUser, Long> {
    List<SystemUser> findByName(String name);
}
